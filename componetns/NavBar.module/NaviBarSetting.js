import React from "react";

// using array, it can be applied with map function
export const NavBarSetting = [
    {
        show_title: "News",
        link_path: "/",
    },
    {
        show_title: "New Arrivals",
        link_path: "/",
    },
    {
        show_title: "Season",
        link_path: "/",
    },
];
