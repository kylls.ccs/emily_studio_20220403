import React from "react";
import style from "./NaviBar.module.css";
import { NavBarSetting } from "./NaviBarSetting";
import { Grid } from "@mui/material";

export default function NaviBar() {
    return (
        <div className={style["nav-bar-container"]}>
            <Grid container>
                <Grid item xs={8} md={8} className={style["nav-bar-logo"]}>
                    <p>EmilyStudio Inc.</p>
                </Grid>
                {NavBarSetting.map((item, index) => {
                    return (
                        <Grid
                            item
                            xs={1}
                            md={1}
                            className={style["nav-bar-subitems"]}
                        >
                            <ul>{item.show_title}</ul>
                        </Grid>
                    );
                })}
            </Grid>
        </div>
    );
}
